<!DOCTYPE html>
<html>
<body>

<h1>Input</h1>
<input type="text" id="givenInput" onkeyup="loadDoc(this.value,'demo')">
<br><br>
<h1>Output</h1>
<p id="demo"></p>

<script>
    var xhttp;
    if(window.XMLHttpRequest){
        //for new version of browser
        xhttp=new XMLHttpRequest();
    }else{
        //for old version of IE browser(IE 5 and IE 6)
        xhttp=new ActiveXObject('Microsoft.XMLHTTP');
    }
    function loadDoc(length, outputId) {
        xhttp.onreadystatechange = function () {
            if (this.readyState == 4 && this.status == 200) {
                document.getElementById("demo").innerHTML = this.responseText;
            }
        };
        searchPage = 'randomPass.php?text=' + length;
        xhttp.open('GET', searchPage);
        xhttp.send();
    }
</script>
</body>
</html>